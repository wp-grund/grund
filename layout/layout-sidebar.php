<?php
global $grund, $grund_theme;

// Get which sidebar to show
$sidebar = $grund->the_query->get( 'sidebar', 'sidebar' );

// Ensure it has widgets
if ( is_active_sidebar( $sidebar ) )
{
    get_header();

    if ( is_archive() || is_home() )
    {
        ?><h1><?=$grund_theme->get_the_page_title()?></h1><?php
    }
    ?>
    <div>
        <?php $grund->the_view(); ?>
    </div>
    <div>
        <?php get_sidebar( $sidebar ) ?>
    </div>
    <?php
    get_footer();
}
else
{
    // Fallback to sidebar-less layout
    $grund->the_query->set( 'layout', null );
    $grund->the_layout();
}

<?php
global $grund;
?>
<article <?=$grund->detail_class()?>>
    <header>
        <h1><?php the_title() ?></h1>
    </header>
    <div>
        <?php the_content() ?>
    </div>
</article>
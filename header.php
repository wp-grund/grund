<?php
global $grund_theme;

// Get custom logo if set
$logo = get_theme_mod( 'custom_logo' );
if ( $logo )
{
    $logo = wp_get_attachment_image( $logo , 'full' );
}
?>
<!DOCTYPE html>
<html>
<head>
    <meta charset="<?php bloginfo( 'charset' ); ?>" />
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title><?=( ( $title = $grund_theme->get_the_page_title() ) ? $title . ' - ' : '' )?><?php bloginfo( 'name' )?></title>
    <?php wp_head() ?>
</head>
<body>
<header id="header">
    <a href="<?=home_url()?>"><?=( $logo ? $logo : get_bloginfo( 'name' ) )?></a>
</header>
